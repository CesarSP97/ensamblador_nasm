;Operaciones con dos numeros estaticos

%macro imprimir 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1     ;permite que la macro resibe 1 parametro y lo remplaza a dicho
        mov edx, %2     ;permite que la macro resiba el valor de impresion y longitud
        int 80h
%endmacro

section .data

    mensajesuma db 10,"Suma: "
    lensuma equ $ - mensajesuma

    mensajeresta db 10,"Resta: "
    lenresta equ $ - mensajeresta

    mensajemul db 10,"Multiplicacion: "
    lenmul equ $ - mensajemul

    mensajediv db 10, "Division: ",10
    lendiv equ $ - mensajediv

    mensajecoc db 10,"Cociente: "
    lencoc equ $ - mensajecoc

    ;new_line db 10, " "

    mensajeres db 10, "Residuo: "
    lenres equ $ - mensajeres

section .bss
        residuo resb 1
        cociente resb 1
        suma resb 1
        resta resb 1
        multiplicacion resb 1

section .text
        global _start
_start:
    
;suma

    imprimir mensajesuma, lensuma
    
    mov al, 4
    mov bl, 2
    add al, bl
    add al, '0'

    mov [suma], al
    
    imprimir suma, 1

;resta

    imprimir mensajeresta, lenres

    mov al, 4
    mov bl, 2
    sub al, bl
    add al, '0'

    mov [resta], al
    
    imprimir resta, 1

;multiplicacion

    imprimir mensajemul, lenmul
    
    mov al, 4
    mov bl, 2
    mul bl
    add al, '0'

    mov [multiplicacion], al
    
    imprimir multiplicacion, 1

;division
    
    imprimir mensajecoc, lencoc

    mov al, 5
    mov bl, 2
    div bl
    add al, '0'
    add ah, '0'

    mov [cociente], al
    mov [residuo], ah

    imprimir cociente, 1
    imprimir mensajecoc, lencoc
    imprimir residuo, 1

    mov eax, 1
    int 80H