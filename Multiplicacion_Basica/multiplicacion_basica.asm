;Imprimir tabla de multiplicar
;Macros y Estatico
;Autor: Cesar Augusto Santin Pinzon
;Fecha: 5 de Agosto de 2020

%macro  imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data 
    inicio db "***Tabla de Multiplicar***", 10
    leng1 equ $ - inicio

    msg1 db " * "
    lengmul equ $ - msg1

    msg2 db " = "
    lengigu equ $ - msg2

    n_linea DB 10,''

section .bss
    a resb 2
    b resb 2 
    c resb 2 

section .text
    global _start

_start:
    imprimir inicio, leng1

    mov al, 3
    add al, '0'
    mov [a], al
    mov cx, 1

ciclo:
    push cx
    mov ax, [a]
    sub ax, '0'
    mul cx
    add ax, '0'
    mov [c], ax
    add cx, '0'
    mov [b], cx

    imprimir a, 1
    imprimir msg1, lengmul
    imprimir b, 1
    imprimir msg2, lengigu
    imprimir c, 1
    imprimir n_linea, 1

    pop cx
    inc cx

    cmp cx, 10
    jnz ciclo

    jmp salir

salir:
    mov eax, 1
    int 80h