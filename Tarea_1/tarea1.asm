;Comentar el significado de cada línea
;Macro se usa para codigo repetido

;La macro que esta definida a continuacion tiene la funcion de imprimir
%macro imprimir 2			;define el nombre de esta macro
	mov eax,4				;instruccion de almacenar 4 en el registro eax para escritura
	mov ebx,1				;instruccion de almacenar 1 en el registro eax
	mov ecx,%1 				;instruccion de referencia a memoria
	mov edx,%2				;instruccion de referencia a escritura
	int 80H					;interrupcion de Sw segun S.O. Linux
%endmacro					;cierra esta macro
;La macro que esta definida a continuacion tiene la funcion de lectura
%macro leer 2				;define el nombre de esta macro
    mov eax,3				;instruccion de almacenar 3 en el registro eax para lectura
    mov ebx,0				;instruccion de almacenar 0 en el registro ebx
    mov ecx,%1  			;instruccion de referencia a memoria
    mov edx,%2 				;instruccion de referencia a lectura
    int 80H 				;interrupcion de Sw segun S.O. Linux
%endmacro					;cierra esta macro

; ecx,modo de acceso
; edx, permisos
section .bss				;apartado para definir datos/variables
	auxiliar resb 30		;instruccion de reserva de 30 bytes
	auxiliarb resb 30		;instruccion de reserva de 30 bytes
	auxiliarc resb 30		;instruccion de reserva de 30 bytes


section .data				;apartado para definir datos/variables
	msg db 0x1b ,"       " 	;instruccion de 7 espacios para contener al dato
	lenmsg equ $-msg		;instruccion referente a la longitud de direccion de la variable msg

	salto db " ",10 		;instruccion de 1 espacio para contener al dato y adiciona un salto de linea
	lensalto equ $-salto	;instruccion referente a la longitud de direccion de la variable salto

section .text				;apartado para definir los procesos del programa, codigo de ensamblado
    global _start			;instruccion para la visibilidad de la etiqueta

_start:						;instruccion para iniciar una nueva etiqueta con sus respectivos procesos
	
	mov ecx,9				;instruccion para almacenar9 en el registro ecx

	mov al,0				;instruccion para realizar el acceso al byte de almacenamiento al
	mov [auxiliar],al		;instruccion para almacenar al en la variable [auxiliar]

cicloI:						;instruccion para iniciar una nueva etiqueta con sus respectivos procesos
	push ecx				;instruccion para almacenar el valor de ecx en la pila
	mov ecx,9				;instruccion para almacenar 9 en el registro ecx

	mov al,0				;instruccion para almacenar 0 en el registro al
	mov [auxiliarb],al		;instruccion para almacenar al en la variable auxiliarb

cicloJ:						;instruccion para iniciar una nueva etiqueta con sus respectivos procesos
	push ecx				;instruccion para almacenar el valor de ecx en la pila

	call imprimir0al9		;instruccion para llamada de una subrutina denominada imprimir0a19

	;imprimir msg2,lenmsg2

fincicloJ:					;instruccion para iniciar una nueva etiqueta con sus respectivos procesos
	mov al,[auxiliarb]		;instruccion para almacenar la variableauxiliarb en el registro al
	inc al					;instruccion para realizar el operando fuente y el operando destino.
	mov [auxiliarb],al		;instruccion para almacenar al en la variable auxiliarb

	pop ecx					;instruccion para cargar el valor de la cima de la pila en ecx
	loop cicloJ				;instruccion que efectua la repeticion del ciclo asignado cicloJ
		
	;imprimir salto,lensalto

fincicloI:					;instruccion para iniciar una nueva etiqueta con sus respectivos procesos
	mov al,[auxiliar]		;instruccion para almacenar auxiliar en el registro al
	inc al					;instruccion para realizar el operando fuente y el operando destino.
	mov [auxiliar],al		;instruccion para almacenar al en la variable auxiliar

	pop ecx					;instruccion para cargar el valor de la cima de la pila en ecx
	loop cicloI				;instruccion que efectua la repeticion del ciclo asignado cicloI

salir:						;instruccion para iniciar una nueva etiqueta con sus respectivos procesos
	mov eax, 1				;instruccion para almacenar 1 en el registro eax
	int 80H					;interrupcion de Sw segun S.O. Linux

imprimir0al9:				;instruccion para iniciar una nueva etiqueta con sus respectivos procesos
	
	mov ebx,"["				;instruccion para almacenar el caracter [ en el registro ebx
	mov [msg+1], ebx		;instruccion de movimiento del registro ebx al valor de la posicion msg asignada 1 

	mov bl,[auxiliar]		;instruccion para almacenar auxiliar en el registro bl 
	add bl,'0'				;instruccion de operacion bl = bl + 0
	mov [msg+2], bl			;instruccion para el movimiento del registro bl al valor de la posicion msg asignada 2 

	mov ebx,";"				;instruccion para almacenar el caracter ; en el registro ebx
	mov [msg+3], ebx		;instruccion de movimiento del registro ebx al valor de la posicion msg asignado 3 
	
	mov bl,[auxiliarb]		;instruccion para almacenar auxiliarb en el registro bl
	add bl,'0'				;instruccion de operacion bl = bl + 0
	mov [msg+4],bl			;instruccion de movimiento del registro bl al valor de la posicion msg asignado 4

	mov ebx,"fJ"			;instruccion para almacenar fJ en el registro ebx
	mov [msg+5], ebx		;instruccion de movimiento del registro ebx al valor de la posicion msg asignado 5

	imprimir msg,lenmsg		;instruccion para imprimir los datos almacenados hasta el punto anterior

	ret						;instruccion de retorno subrutina