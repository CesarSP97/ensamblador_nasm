section .data
    msj1 db "***Ingrese 5 numeros***", 10
    len1 equ $ - msj1

    msj2 db "El numero mayor es: "
    len2 equ $ - msj2

    arreglo db 0,0,0,0
    len_arreglo equ $ - arreglo

section .bss
    numero resb 2
    dato resb 1

section .data
    global _start

_start:
    mov esi, arreglo        ;00000
    mov edi, 0

    ;*************mensaje 1*************
    mov eax, 4
    mov ebx, 1
    mov ecx, msj1
    mov edx, len1
    int 80h

    ;******** Lectura de datos en el arreglo ********

leer:
    mov eax, 3
    mov ebx, 0
    mov ecx, numero
    mov edx, 2
    int 80h

    mov al, [numero]
    sub al, '0'

    ;******** movemos el valor a un indice del arreglo ******** 
    mov [esi], al       

    inc edi
    inc esi
    cmp edi, len_arreglo
    jb leer
    mov ecx, 0
    mov bl, 0

comparacion:
    mov al, [arreglo + ecx]
    cmp al, bl
    jb contador
    mov bl, al

contador:
    inc ecx
    cmp ecx, len_arreglo
    jb comparacion

imprimir:
    mov bl, '0'
    add [numero], bl

    mov eax, 4
    mov ebx, 1
    mov ecx, msj2
    mov edx, len2
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, dato
    mov edx, 1
    int 80h

salir:
    mov eax, 1
    int 80h
