%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    asterisco db '*'
    n_linea db 10,''

section .text
    global _start

_start:
    mov ecx, 9

l1:
    push ecx    ;Filas
    push ecx    ;Columnas

    imprimir n_linea, 1
    
    pop ecx
    mov eax, 9
    ;sub eax, ecx
    mov ecx, eax

l2:
    push ecx
    
    imprimir asterisco, 1

    pop ecx
    loop l2
    pop ecx
    loop l1

    jmp salir

salir:
    mov eax, 1
    int 80h

