%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

%macro  leer 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    msg1		db		10,'---Calculadora---',10,0
	lmsg1		equ		$ - msg1

	msg2		db		10,'Numero 1: ',0
	lmsg2		equ		$ - msg2
 
	msg3		db		10,'Numero 2: ',0
	lmsg3		equ		$ - msg3

    msg4		db		10,'1) Sumar',0
	lmsg4		equ		$ - msg4

    msg5		db		10,'2) Restar',0
	lmsg5		equ		$ - msg5

    msg6		db		10,'3) Multiplicar',0
	lmsg6		equ		$ - msg6

    msg7		db		10,'4) Dividir',0
	lmsg7		equ		$ - msg7

    msg8		db		10,'5) Salir',0
	lmsg8		equ		$ - msg8

    msg9		db		10,'Resultado: ',0
	lmsg9		equ		$ - msg9

    msg11		db		10,'Seleccione una opcion: ',0
	lmsg11		equ		$ - msg11

    msg10		db		10,'Opcion Invalida',10,0
	lmsg10		equ		$ - msg10

    new_line db "",10

section .bss
    opcion:     resb    2
  	num1:		resb	2
	num2:		resb 	2
	resultado:	resb 	2

section .text
    global _start

_start:
    ;Mensaje de titulo
    imprimir msg1, lmsg1

    ;Mensaje para numeros
    imprimir msg2, lmsg2
    leer num1, 2

    imprimir msg3, lmsg3
    leer num2, 2

    ;Mensaje del menu
    imprimir msg4, lmsg4    ;sumar
    imprimir msg5, lmsg5    ;restar
    imprimir msg6, lmsg6    ;multiplicar
    imprimir msg7, lmsg7    ;dividir
    imprimir msg8, lmsg8    ;salir

    jmp opciones

opciones:
    ;Mensaje de opcion
    imprimir msg11, lmsg11
    leer opcion, 2

    mov ah, [opcion]
    sub ah, '0'

    ; Comparar el valor ingresado por el usuario, para saber que operacion se desea realizar
    ;JE = jmp (condicional) if exist iqual, salta cuando los valores de los 2 operandos son iguales
    ; ZF = 1, CF = 0
    cmp ah, 1
    JE sumar

    cmp ah, 2
    JE restar

    cmp ah, 3
    JE multiplicar
    
    cmp ah, 4
    JE dividir

    cmp ah, 5 
    JE salir

    imprimir msg10,lmsg10
    jmp salir

sumar:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Sumamos el registro AL y BL
	add al, bl
;	aaa
 
	; Convertimos el resultado de la suma de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al
 
	; Imprimimos en pantalla el mensaje 9
    imprimir msg9, lmsg9
 
	; Imprimimos en pantalla el resultado
	imprimir resultado, 2
 
	jmp opciones
 
restar:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Restamos el registro AL y BL
	sub al, bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al
 
	; Imprimimos en pantalla el mensaje 9
    imprimir msg9, lmsg9
 
	; Imprimimos en pantalla el resultado
	imprimir resultado, 2
 
	jmp opciones

multiplicar:
 
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Multiplicamos. AX = AL X BL
	mul bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], ax
 
	; Imprimimos en pantalla el mensaje 9
    imprimir msg9, lmsg9
 
	; Imprimimos en pantalla el resultado
	imprimir resultado, 2
 
	jmp opciones

dividir:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Igualamos a cero los registros DX y AH
	mov dx, 0
	mov ah, 0
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Division. AL = AX / BL. AX = AH:AL
	div bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], ax
 
	; Imprimimos en pantalla el mensaje 9
    imprimir msg9, lmsg9
 
	; Imprimimos en pantalla el resultado
	imprimir resultado, 2
 
	jmp opciones

salir:
    imprimir new_line, 1

    ; Finalizamos el programa
	mov eax, 1
	mov ebx, 0
	int 80h