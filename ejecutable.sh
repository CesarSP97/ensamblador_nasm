#ARQUITECTURA DE 32 BITS

#if [ $# -eq 1 ]
#then 
#        echo "...Compilando y ejecutando..."
#        nasm -f elf $1.asm; ld -m elf_i386 -s -o $1 $1.o
#        ./$1
#else
#        echo ".............................."
#        echo "No compila"
#        echo ".............................."
#fi

#ARQUITECTURA DE 64 BITS

if [ $# -eq 1 ]
then 
        echo "...Compilando y ejecutando..."
        nasm -f elf64 -o $1.o $1.asm; ld -o $1 $1.o
        ./$1
else
        echo ".............................."
        echo "No compila"
        echo ".............................."
fi