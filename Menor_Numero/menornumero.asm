section .data
    msj1 db "***Ingrese 5 numeros***", 10
    len1 equ $ - msj1

    msj2 db "El numero menor es: ", 10
    len2 equ $ - msj2

    arreglo db 0,0,0,0
    len_arreglo equ $ - arreglo

section .bss
    numero resb 2
    menor resb 1
    

section .data
    global _start

_start:
    mov esi, arreglo        ;00000
    mov edi, 0

    ;*************mensaje 1*************
    mov eax, 4
    mov ebx, 1
    mov ecx, msj1
    mov edx, len1
    int 80h

    ;******** Lectura de datos en el arreglo ********

leer:
    mov eax, 3
    mov ebx, 0
    mov ecx, numero
    mov edx, 2
    int 80h
    ;******** asignacion de un numero en el arreglo ******** 
    mov eax, [numero]
    sub eax, '0'
    mov [esi], eax

    ;******** incrementar una posicion en e arreglo ******** 
    add esi, 1
    add edi, 1
    
    ;******** comparar para ahcer el salto ******** 
    cmp edi, len_arreglo
    jb leer

    mov esi, arreglo
    mov edi, 0

    mov al, [esi]
    mov [menor], al

nmenor:
    mov bl, [esi]
    add esi, 1
    add edi, 1

    mov al, [menor]

    cmp bl, al
    jb  numero_menor
    jmp comparacion

numero_menor:
    mov [menor],bl

comparacion:
    cmp edi, len_arreglo
    jb nmenor

imprimir:
    mov eax, 4
    mov ebx, 1
    mov ecx, msj2
    mov edx, len2
    int 80h

    mov eax, [menor]
    add eax, '0'
    mov [menor], eax

    mov eax, 4
    mov ebx, 1
    mov ecx, menor
    mov edx, 1
    int 80h

salir:
    mov eax, 1
    int 80h