%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    mensaje DB "*** Ejemplo CALL ***"
    len_mensaje EQU  $ - mensaje

    mensaje1 DB "Su nombre es:"
    len_mensaje1 EQU  $ - mensaje1

    mensaje2 DB "Cesar Santin"
    len_mensaje2 EQU  $ - mensaje2

    n_espacio DB ' '

    n_linea DB 10,''

section .text
    global _start

_start:

    imprimir mensaje, len_mensaje
    call nueva_linea

    imprimir mensaje1, len_mensaje1
    call espacio

    imprimir mensaje2, len_mensaje2
    
    jmp salir

espacio:
    imprimir n_espacio, 1
	ret

nueva_linea:
	imprimir n_linea, 1
	ret

salir:
    mov eax, 1
	int 80h