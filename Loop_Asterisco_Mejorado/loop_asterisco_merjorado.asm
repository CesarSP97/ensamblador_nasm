section .data

    asterisco DB '*'
    n_linea DB 10,''

section .text
    global _start

_start:
    mov ecx, 10
    mov ebx, 1

l1:
    push ecx
    push ebx
    mov ecx, ebx

l2:
    push ecx
    ;***********asterisco**********
    mov eax, 4
    mov ebx, 1
    mov ecx, asterisco
    mov edx, 1
    int 80h

    pop ecx
    loop l2             ;salto a l2, dec cx
    ;***********finaliza loop columnas**********
    
    ;**************enter**********
    mov eax, 4
    mov ebx, 1
    mov ecx, n_linea
    mov edx, 1
    int 80h

    pop ebx
    pop ecx
    inc ebx
    loop l1
    ;***********finaliza loop filas**********

    jmp salir

salir:
    mov eax, 1
    int 80h