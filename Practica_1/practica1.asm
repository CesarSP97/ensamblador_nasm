section .data
    nombres DB "Nombres: Cesar Augusto", 10      ;constante mensaje de un byte de memoria
    longitud1 EQU $-nombres             ;constante logitud que calcula el # de caracteres del mensaje
        
    apellidos DB "Apellidos: Santin Pinzon", 10    ;constante mensaje de un byte de memoria
    longitud2 EQU $-apellidos           ;constante logitud que calcula el # de caracteres del mensaje

    materia DB "Materia: Lenguaje Ensamblador", 10 ;constante mensaje de un byte de memoria
    longitud3 EQU $-materia             ;constante logitud que calcula el # de caracteres del mensaje

    genero DB "Genero: Masculino", 10           ;constante mensaje de un byte de memoria
    longitud4 EQU $-genero              ;constante logitud que calcula el # de caracteres del mensaje
    
    ; ***** section/segment se puede utilizar ambos *****

;section .bss

section .text
    global _start                           ;define etiqueta
_start:
    ; ***** imprimir el mensaje *****
    mov eax, 4              ;inicialmente por lo pronto es un movimiento de un dato al registro eax
                            ;tipo de subrutina, operacion => escritura, salida
    mov ebx, 1              ;inicialmente por lo pronto es un movimiento de un dato al registro ebx
                            ;tipo de estandar, por teclado
    mov ecx, nombres        ;inicialmente por lo pronto es un movimiento de un dato al registro ecx
                            ;en ecx se almacena la referencia a imprimir "mensaje" (sin corchetes solo es referencia)
    mov edx, longitud1      ;inicialmente por lo pronto es un movimiento de un dato al registro edx
                            ;en ecx se almacena la referencia a imprimir por numero de caracteres
    int 80H

    mov eax, 4              ;inicialmente por lo pronto es un movimiento de un dato al registro eax
                            ;tipo de subrutina, operacion => escritura, salida
    mov ebx, 1              ;inicialmente por lo pronto es un movimiento de un dato al registro ebx
                            ;tipo de estandar, por teclado
    mov ecx, apellidos        ;inicialmente por lo pronto es un movimiento de un dato al registro ecx
                            ;en ecx se almacena la referencia a imprimir "mensaje" (sin corchetes solo es referencia)
    mov edx, longitud2      ;inicialmente por lo pronto es un movimiento de un dato al registro edx
                            ;en ecx se almacena la referencia a imprimir por numero de caracteres
    int 80H

    mov eax, 4              ;inicialmente por lo pronto es un movimiento de un dato al registro eax
                            ;tipo de subrutina, operacion => escritura, salida
    mov ebx, 1              ;inicialmente por lo pronto es un movimiento de un dato al registro ebx
                            ;tipo de estandar, por teclado
    mov ecx, materia        ;inicialmente por lo pronto es un movimiento de un dato al registro ecx
                            ;en ecx se almacena la referencia a imprimir "mensaje" (sin corchetes solo es referencia)
    mov edx, longitud3      ;inicialmente por lo pronto es un movimiento de un dato al registro edx
                            ;en ecx se almacena la referencia a imprimir por numero de caracteres
    int 80H

    mov eax, 4              ;inicialmente por lo pronto es un movimiento de un dato al registro eax
                            ;tipo de subrutina, operacion => escritura, salida
    mov ebx, 1              ;inicialmente por lo pronto es un movimiento de un dato al registro ebx
                            ;tipo de estandar, por teclado
    mov ecx, genero        ;inicialmente por lo pronto es un movimiento de un dato al registro ecx
                            ;en ecx se almacena la referencia a imprimir "mensaje" (sin corchetes solo es referencia)
    mov edx, longitud4      ;inicialmente por lo pronto es un movimiento de un dato al registro edx
                            ;en ecx se almacena la referencia a imprimir por numero de caracteres
    int 80H                 ;interrupcion  de Sw para el S.O. Linux

    mov eax, 1              ;junto a la interrupcion = salida del programa, system_exit, sys_exit
    mov ebx, 0              ;si el retorno es 0 (200 en la web) codigo OK
    int 80H
