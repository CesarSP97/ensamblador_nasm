;Nombre: Cesar Augusto Santin Pinzon
;Fecha: 22 Julio del 2020

;Programa que imprime una matriz de asteriscos

section .data
    asterisco db '* '
    n_linea db 10,' '


section .text
    global _start

_start:
    mov eax, 5
    mov ebx, 5
    jmp filas

filas:
    push eax                ;eax siempre 5
    push ebx                ;ebx siempre 5

    mov eax, 4
    mov ebx, 1
    mov ecx, asterisco
    mov edx, 1
    int 80H

    pop ebx
    pop eax
    
    dec eax

    cmp eax, 0

    jz columnas
    jmp filas

columnas:
    dec ebx
    push ebx

    mov eax, 4
    mov ebx, 1
    mov ecx, n_linea
    mov edx, 1
    int 80H

    pop ebx
    mov eax, 5

    cmp ebx,0

    jz salir
    jmp filas

salir:
    mov eax, 1
    int 80h