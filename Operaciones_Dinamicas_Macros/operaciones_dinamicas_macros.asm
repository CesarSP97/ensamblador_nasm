;Nombre: Cesar Augusto Santin Pinzon
;Fecha: 22/06/2020

;Operaciones con numeros dinamicos usando macros

%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1     ;permite que la macro resibe 1 parametro y lo remplaza a dicho
    mov edx, %2     ;permite que la macro resiba el valor de impresion y longitud
    int 80h
%endmacro

%macro  leer 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1     ;permite que la macro resibe 1 parametro y lo remplaza a dicho
    mov edx, %2     ;permite que la macro resiba el valor de impresion y longitud
    int 80h
%endmacro

section .data
    
    mensaje db "Operaciones Dinamicas MACROS", 10
    leng equ $-mensaje

    ;***** Ingreso de Numeros *****
    msjnumero1 db "Ingrese el primer numero: "
    lengnum1 equ $ - msjnumero1

    msjnumero2 db "Ingrese el segundo numero: "
    lengnum2 equ $ - msjnumero2

    ;***** Suma *****
    mjsSuma db "La suma es: "
    lengSuma equ $ - mjsSuma

    ;***** Resta *****
    mjsResta db "La resta es: "
    lengResta equ $ - mjsResta

    ;***** Multiplicacion *****
    mjsMul db "La multiplicación es: "
    lengMul equ $ - mjsMul

    ;***** Division *****
    mjsDiv db "---Division---",10
    lengDiv equ $ - mjsDiv
    
    ;***** Cociente *****
    msjDiv1 db "El cociente es: "
    lengDiv1 equ $-msjDiv1
    
    ;***** Residuo *****
    msjDiv2 db "El residuo es: "
    lengDiv2 equ $-msjDiv2

    new_line db "",10

section .bss
    numero1 resb 1
    numero2 resb 1
    suma resb 1
    resta resb 1
    multiplicacion resb 1
    cociente resb 1
    residuo resb 1

section .text
    global _start

_start:

    imprimir mensaje, leng

    ;***** Obtener numero 1 (Entrada) *****
    imprimir msjnumero1, lengnum1
    leer numero1, 2

    ;***** Obtener numero 2 (Entrada) *****
    imprimir msjnumero2, lengnum2
    leer numero2, 2

    ;***** Proceso Suma *****
    mov ax,[numero1]
    mov bx,[numero2]
    sub ax, '0'                     ;convertir de cadena a digito
    sub bx, '0'                     ;convertir de cadena a digito
    add ax, bx
    add ax, '0'
    mov [suma], ax 
    
    imprimir mjsSuma, lengSuma
    imprimir suma, 1
    imprimir new_line, 1

    ;***** Proceso Resta *****
    mov ax,[numero1]
    mov bx,[numero2]
    sub ax, '0'                     ;convertir de cadena a digito
    sub bx, '0'                     ;convertir de cadena a digito
    sub ax, bx
    add ax, '0'
    mov [resta], ax 
    
    imprimir mjsResta, lengResta
    imprimir resta, 1
    imprimir new_line, 1

    ;***** Proceso Multiplicacion *****
    mov al,[numero1]
    mov bl,[numero2]
    sub al, '0'                     ;convertir de cadena a digito
    sub bl, '0'                     ;convertir de cadena a digito
    mul bl
    add al, '0'
    mov [multiplicacion], al 
    
    imprimir mjsMul, lengMul
    imprimir multiplicacion, 1
    imprimir new_line, 1

    ;***** Proceso Division *****
    mov al, [numero1]
    mov bl, [numero2]
    sub al, '0'                     ;convertir de cadena a digito
    sub bl, '0'                     ;convertir de cadena a digito
    div bl
    add al, '0'
    mov [cociente], al
    add ah, '0'
    mov [residuo], ah
    
    imprimir mjsDiv, lengDiv
    ;***** Cociente *****
    imprimir msjDiv1, lengDiv1
    imprimir cociente, 1
    imprimir new_line, 1
    ;***** Residuo *****
    imprimir msjDiv2, lengDiv2
    imprimir residuo, 1
    imprimir new_line, 1
    
    mov eax, 1
    int 80h