


section .data
    mensaje db	10, 'Interrupcion Exitosa', 10  ;constante almacenada en un byte de memoria
	leng equ $-mensaje                          ;constante que calcula el numero de caracteres
    
section .text
    global _start

_start:
    ;***Encabezado***
    mov eax, 4          ;instruccion que permite aplicar el operando de escritura
    mov ebx, 1          ;instruccion que permite el ingreso por teclado
    mov ecx, mensaje   ;instruccion que almacena la referencia para imprimir (mensaje1)
    mov edx, leng  ;instruccion que almacena la referencia para imprimir por numero de caracteres
    int 80H             ;interrupcion dependiente del sistema operativo

    ;mov eax, 1          ;instruccion para salida del programa, system_exit, sys_exit
    ;int 80H

    
    mov eax, ebx            ;almacena los datos de ebx en eax
    mov bx, 20              ;en el registro de 16 bits bx se almacena el valor de 20
    mov ax, [di]            ;almacena el valor en la dirección de memoria especificada por di, en ax
    mov al,[auxiliar]		;almacenar la variableauxiliar en el registro al 
    mov [1000h], ax         ;en la dirección 1000h de la memoria se obtiene el valor de ax
    