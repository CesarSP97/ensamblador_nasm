%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    msj db '*'
    n_linea db 10,''

section .text
    global _start

_start:
    mov ecx, 9          ;filas
    mov ebx, 9          ;columnas

principal:
    push ebx
    cmp ebx, 0          ;ebx = -1
    ;pop ebx
    jz salir
    jmp asterisco

asterisco:
    dec ecx
    push ecx
    imprimir msj, 1     ;el valor de ecx se remplaza con msj = '*'
    pop ecx
    cmp ecx, 0
    jg asterisco        ;jg, veriica que el primer operando sea mayor que el segundo
    imprimir n_linea, 1
    pop ebx
    dec ebx
    mov  ecx, 9
    jmp principal

salir:
    mov eax, 1
    int 80h