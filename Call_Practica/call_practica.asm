%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

%macro leer 2 
    mov eax, 3
    mov ebx, 0
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data

    mensaje DB "*** Ejemplo CALL ***"
    len_mensaje EQU  $ - mensaje

    ingreso_n DB "Ingrese su nombre completo:"
    len_ingreso_n EQU  $ - ingreso_n

    mensaje1 DB "Usted se llama:"
    len_mensaje1 EQU  $ - mensaje1

    n_espacio DB " "

    n_linea DB 10,""

section .bss
    datos: resb 1

section .text
    global _start

_start:

    imprimir mensaje, len_mensaje
    call nueva_linea

    imprimir ingreso_n, len_ingreso_n
    call espacio
    leer datos, 256

    call nueva_linea

    imprimir mensaje1, len_mensaje1
    call espacio

    mov ax, [datos]
    imprimir datos, 256
    
    jmp salir

espacio:
    mov eax, 4
	mov ebx, 1
	mov ecx, n_espacio
	mov edx, 1
	int 80h

	ret

nueva_linea:
	mov eax, 4
	mov ebx, 1
	mov ecx, n_linea
	mov edx, 1
	int 80h
	
	ret

salir:
    mov eax, 1
	int 80h