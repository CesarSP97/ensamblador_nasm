%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    msg db "***Tablas de Multiplicar***", 10
    len_msg equ $ - msg

    msg1 db ' * '
    len_msg1 equ $ - msg1

    msg2 db ' = '
    len_msg2 equ $ - msg2

    msg3 db ' | '
    len_msg3 equ $ - msg3
    
    n_espacio db 10
    len_n_espacio equ $ - n_espacio

section .bss
    numero resb 2
    presentarN1 resb 2
    presentarN2 resb 2
    respuesta resb 2
    aux resb 2
    x resb 2

section .text
        global _start

_start:
    imprimir msg, len_msg

    mov al, 1
    mov [aux], al
    mov ecx, 0

principal:
    cmp ecx, 9
    jz bucle
    inc ecx
    push ecx
    mov [numero], ecx
    jmp imp

imp:
    mov al, [numero]
    mov [presentarN1], al
    mov cl, [aux]
    mul cl

    mov [numero], cl
    mov ah, [presentarN1]
    add ax, '00'
    add cx, '0'
    mov [presentarN1], ah
    mov [presentarN2], cl

    imprimir presentarN1, 1
    imprimir msg1, len_msg1
    imprimir presentarN2, 1
    imprimir msg2, len_msg2

    mov eax, 48
    add [numero], eax

    imprimir numero, 2
    imprimir n_espacio, 1

    pop ecx
    jmp principal

bucle:
    imprimir n_espacio, len_n_espacio
    mov ebx, [aux]
    inc ebx
    mov [aux], ebx
    mov ecx, 0
    cmp ebx, 9
    jb principal                    ; jnAE

    jmp salir

salir:
    mov eax, 1
    int 80h