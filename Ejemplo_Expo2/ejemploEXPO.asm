section .data
    x      db 5                ; Declaramos las variables que utilizaremos
    result db 0
 
section .text
 
    global _start                ; Hacemos visible la etiqueta main
 
_start:

factorial:
    push rbp                   ; Almacenar el registro que usaremos de apuntador
                               ; a la pila rbp
    mov rbp, rsp               ; Asignar a rbp el valor del registro apuntador rsp
 
    push rax                   ; Almacenamos en la pila los registros
    push rbx                   ; modificados dentro de la subrutina
 
    mov rax, 1                 ; rax será el resultado
    mov rbx, qword[x]          ; Calculamos el factorial de la variable 'x' (=5).
while:
    cmp rbx, 1                 ; Hacemos la comparación
    jle fin                     ; Si se cumple la condición saltamos a fin
    imul rax, rbx
    dec rbx
    jmp while                  ; Salta a while
fin:                            ; En rax tendremos el valor del factorial de 'x' (=120)
    mov qword[result], rax     ; Almacenamos el resultado en la variable 'result'
 
    pop rbx                    ; Restauramos el valor inicial de los registros
    pop rax                    ; en orden inverso a como los hemos almacenado
    
    mov rsp, rbp               ; Restauramos el valor inicial de rsp con rbp
    pop rbp                    ; Restauramos el valor inicial de rbp
 
    ret                        ; Finaliza la ejecución de la subrutina
 
main:
    call factorial             ; Llamamos a la subrutina factorial
    
    mov rax, 1
    mov rbx, 0

    ;mov eax, 1
    int 80h                    ; Finaliza la ejecución del programa